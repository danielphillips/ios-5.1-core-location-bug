//
//  ViewController.h
//  locatiombug
//
//  Created by Daniel Phillips on 25/04/2012.
//  Copyright (c) 2012 KoffeeCup. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate>
{
    CLLocationManager * lm;
}
- (IBAction)stop:(id)sender;
@end
