//
//  ViewController.m
//  locatiombug
//
//  Created by Daniel Phillips on 25/04/2012.
//  Copyright (c) 2012 KoffeeCup. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    lm = [[CLLocationManager alloc] init];
    lm.delegate = self;
    [lm startUpdatingLocation];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Location Manager Delegate methods
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"didChangeAuthorizationStatus:");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    NSLog(@"didUpdateToLocation:fromLocation:");
}

- (IBAction)stop:(id)sender {
    [lm stopUpdatingLocation];
    ((UIButton*)sender).enabled = NO;
}

@end
