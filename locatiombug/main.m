//
//  main.m
//  locatiombug
//
//  Created by Daniel Phillips on 25/04/2012.
//  Copyright (c) 2012 KoffeeCup. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
