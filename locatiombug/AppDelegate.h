//
//  AppDelegate.h
//  locatiombug
//
//  Created by Daniel Phillips on 25/04/2012.
//  Copyright (c) 2012 KoffeeCup. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
